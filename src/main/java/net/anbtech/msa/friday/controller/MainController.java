package net.anbtech.msa.friday.controller;
// push test
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import net.anbtech.msa.core.entity.SystemLog;
import net.anbtech.msa.core.mybatis.repository.UserMybatisRepository;
import net.anbtech.msa.core.repository.SystemLogRepository;

/**
 * @author restnfeel
 * 
 * 
 */
@Transactional
@CrossOrigin	
@RestController
@RequestMapping(value="/api/friday/main")
public class MainController {

	
	/**
	 * @return
	 * 로그인 정보조회
	 */
	@GetMapping
	@Secured({"ROLE_ADMIN","ROLE_MANAGER","ROLE_USER"})
	public ResponseEntity<List<SystemLog>> basicR() {
		
		List<SystemLog> list = new ArrayList<>();
		
		for(int i =0; i<100; i++){
			SystemLog vo = new SystemLog();
			vo.setLogData("Mock 데이터 "+i+" 번째 정보입니다.");
			list.add(vo);
		}
		
        return new ResponseEntity<List<SystemLog>>(list, HttpStatus.OK);
	}
	
	
}
