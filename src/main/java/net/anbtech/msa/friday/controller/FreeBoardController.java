package net.anbtech.msa.friday.controller;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import net.anbtech.msa.core.util.ReflectPojoSetUtil;
import net.anbtech.msa.friday.entity.FreeBoard;
import net.anbtech.msa.friday.repository.FreeBoardRepository;

//소스 push test 임준영2
// test2223
//test
@CrossOrigin	
@RestController
@RequestMapping(value="/api/friday/freeboard")
@Transactional
public class FreeBoardController {

private static final Logger LOG = LoggerFactory.getLogger(FreeBoardController.class);
	
	@Autowired
	private FreeBoardRepository repo_board;

	
	/**
	 * board 전체 조회
	 * @return
	 */
	@GetMapping
	public ResponseEntity<List<FreeBoard>> basicRFreeBoard() {
		
		List<FreeBoard> list = repo_board.findAll();
		
        return new ResponseEntity<List<FreeBoard>>(list, HttpStatus.OK);
	}
	
	
	/**
	 * board 저장
	 * @param vo
	 * @param ucBuilder
	 * @return
	 */
	@PostMapping
    public ResponseEntity<Void> basicCFreeBoard(@RequestBody FreeBoard vo,  UriComponentsBuilder ucBuilder) {
	
		repo_board.save(vo);
		
		HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }
	
	
	/**
	 * board 수정
	 * @param vo
	 * @param ucBuilder
	 * @return
	 */
	@PatchMapping
    public ResponseEntity<Void> basicUFreeBoard(@RequestBody FreeBoard vo,  UriComponentsBuilder ucBuilder) {
		
		
	    FreeBoard po = repo_board.findOne(vo.getFbId());
	
		try {
			po = (FreeBoard) ReflectPojoSetUtil.setPojo(po, vo);
		} catch (Exception e) {
			e.printStackTrace();
		}			
		repo_board.save(po);

        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }
	
	
	
	/**
	 * board 삭제
	 * @param id
	 * @return
	 */
	@DeleteMapping(value="/{id}")
    public ResponseEntity<FreeBoard> basicDFreeBoard(@PathVariable("id") String id ) {
	    
	    FreeBoard vo = repo_board.findOne(id);
		
        if (vo == null) {
            return new ResponseEntity<FreeBoard>(HttpStatus.NOT_FOUND);
        }
 
        repo_board.delete(id);
        
        return new ResponseEntity<FreeBoard>(HttpStatus.NO_CONTENT);
    }
	
	
}
