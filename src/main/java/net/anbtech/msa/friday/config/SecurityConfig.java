package net.anbtech.msa.friday.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

@Configuration
@EnableResourceServer
public class SecurityConfig extends WebSecurityConfigurerAdapter {



	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			.csrf().disable()
				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
				.authorizeRequests()
					.antMatchers("/login").permitAll()
					.antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
					// 개발자 API 호출
					.antMatchers("/memreg/*").permitAll()
					//.antMatchers("/api/**").hasAuthority("ROLE_ADMIN")
					// .antMatchers("/api/**").hasAnyAuthority("ROLE_ADMIN")
					// 아래 디폴트
					.anyRequest().authenticated()
					.and()
				// .formLogin()
				// .and()
				.logout();
	}
	
	@Override
	public void configure(WebSecurity web) throws Exception {
		// TODO Auto-generated method stub
		//super.configure(web);
		web.ignoring().antMatchers("/v2/api-docs", "/configuration/ui", "/swagger-resources", "/configuration/security", 
				"/swagger-ui.html", "/api/friday/**");
	}




}

