package net.anbtech.msa.friday.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import org.springframework.session.web.http.HeaderHttpSessionStrategy;
import org.springframework.session.web.http.HttpSessionStrategy;

@Configuration
@EnableRedisHttpSession
public class HttpSessionConfig {

    @Bean
    public JedisConnectionFactory connectionFactory() {
    	JedisConnectionFactory jc = new JedisConnectionFactory();
    	// Redis info
    	jc.setHostName("api.anbtech.net");
    	jc.setPassword("dpdldosq13");
    	jc.setPort(6379);
    	jc.setUsePool(true);
    	
        return jc;
    }

    @Bean
    public HttpSessionStrategy httpSessionStrategy() {
        return new HeaderHttpSessionStrategy();
    }

}