package net.anbtech.msa.friday;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication //test2
public class AnbtechMsaFridayApplication {

	public static void main(String[] args) {
		SpringApplication.run(AnbtechMsaFridayApplication.class, args);
	}
}
