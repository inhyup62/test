package net.anbtech.msa.friday.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import net.anbtech.msa.friday.entity.FridayAreaMaster;

public interface FridayAreaMasterRepository extends JpaRepository<FridayAreaMaster, String>{

}
