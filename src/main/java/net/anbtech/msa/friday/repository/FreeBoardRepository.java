package net.anbtech.msa.friday.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import net.anbtech.msa.friday.entity.FreeBoard;

public interface FreeBoardRepository extends JpaRepository<FreeBoard, String>{

}
