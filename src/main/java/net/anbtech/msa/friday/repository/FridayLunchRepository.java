package net.anbtech.msa.friday.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import net.anbtech.msa.friday.entity.FridayLunch;

public interface FridayLunchRepository extends JpaRepository<FridayLunch, String>{

}
