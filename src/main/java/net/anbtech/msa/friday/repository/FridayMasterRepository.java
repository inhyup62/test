package net.anbtech.msa.friday.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import net.anbtech.msa.friday.entity.FridayMaster;

public interface FridayMasterRepository extends JpaRepository<FridayMaster, String>{

}
