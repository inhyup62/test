package net.anbtech.msa.friday.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Data
@Entity
@Table
public class FreeBoard {
	
	@Id
	@GenericGenerator(name="id_generator", strategy="net.anbtech.msa.core.generator.RnfIdInGenerator")
	@GeneratedValue(generator="id_generator")
	@Column(name="fb_id", unique=true, nullable=false, length=100)
	private String fbId;
	
	private String fbTitle;
	
	@Lob
	private String fbContent;
	
	@Temporal(TemporalType.DATE)
	private Date wrDate;
	
	private String wrId;
	

}
