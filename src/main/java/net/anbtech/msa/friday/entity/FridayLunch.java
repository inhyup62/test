package net.anbtech.msa.friday.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

/**
 * @author restnfeel
 * 
 * 
 */
@Data
@Entity
@Table
public class FridayLunch {

	@Id
	@GenericGenerator(name="id_generator", strategy="net.anbtech.msa.core.generator.RnfIdInGenerator")
	@GeneratedValue(generator="id_generator")
	@Column(name="fl_id", unique=true, nullable=false, length=100)
	private String flId;
	
	private String flUserId;
	
	@Temporal(TemporalType.DATE)
	private Date flData;
	
	private Boolean isOk;
	
	private String notReason;
	
	@ManyToOne(targetEntity=FridayAreaMaster.class, fetch=FetchType.LAZY)
	@JoinColumn(name="fa_id")
	private FridayAreaMaster areaCode;
	
	private Boolean isCheck;
	
}
