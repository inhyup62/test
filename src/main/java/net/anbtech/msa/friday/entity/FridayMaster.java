package net.anbtech.msa.friday.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Data
@Entity
@Table
public class FridayMaster {

	@Id
	@GenericGenerator(name="id_generator", strategy="net.anbtech.msa.core.generator.RnfIdInGenerator")
	@GeneratedValue(generator="id_generator")
	@Column(name="fm_id", unique=true, nullable=false, length=100)
	private String fmId;
		
	@Temporal(TemporalType.DATE)
    private Date checkDate;

	@Column(name="check_user_id", nullable=false, length=50)
	private String checkUserId;
	
	@Column(name="check_area", length=200)
	private String checkArea;
	
	private Integer destUserCount;
	
	private Integer checkedUserCount;
	
	@OneToMany(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="fl_id")
	private List<FridayLunch> flUsers;
	
	
}
