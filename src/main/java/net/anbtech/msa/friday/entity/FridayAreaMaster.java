package net.anbtech.msa.friday.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Data
@Entity
@Table
public class FridayAreaMaster {

	@Id
	@GenericGenerator(name="id_generator", strategy="net.anbtech.msa.core.generator.RnfIdInGenerator")
	@GeneratedValue(generator="id_generator")
	@Column(name="fa_id", unique=true, nullable=false, length=100)
	private String faId;
	
	private String checkUserId;
	
	private String areaName;
	
	private Date udateDate;
	
	private String updateUserId;
	
}
